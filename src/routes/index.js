const { Router } = require('express')
const nodemailer = require('nodemailer')
const router = Router()
const fs = require("fs")

/* para recibir el correo de test hay que habilitar en este link el acceso de aplicaciones poco seguras: https://myaccount.google.com/lesssecureapps?pli=1 */

router.post('/send-email', async (req, res)=>{

    NOMBRE_ARCHIVO = "./template/template.html"
    const {email} = req.body

    fs.readFile(NOMBRE_ARCHIVO, 'utf8', (error, datos) => {
        if (error) throw error;

        const transporter = nodemailer.createTransport({
            service: 'gmail',
            secure: false,
            auth: {
                user: '',//poner usuario y contraseña del servidor de mensajes de salida
                pass: ''
            },
        })

        transporter.sendMail ({
            from: '',
            to: `${email}`,
            subject: 'test',
            html: datos
          });

    });

    res.redirect('/success.html');

})

module.exports = router;